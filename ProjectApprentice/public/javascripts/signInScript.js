(function(){

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyA6633g9oUGKDZWXQrHXEV0ltqDNtpXCR0",
    authDomain: "project-apprentice.firebaseapp.com",
    databaseURL: "https://project-apprentice.firebaseio.com",
    storageBucket: "project-apprentice.appspot.com",
    messagingSenderId: "978941437309"
  };
  firebase.initializeApp(config);

 // var secondApp = firebase.initializeApp({config}, "Secondary");


  const auth = firebase.auth();
  // Get elements
  const signInBtn = document.getElementById('signInBtn');
  const registerBtn = document.getElementById('regBtn');
  const inputEmail = document.getElementById('inputEmail');
  const inputPassword = document.getElementById('inputPassword');
  const signOutBtn = document.getElementById('signOutBtn');

  //logs in the user
  signInBtn.addEventListener('click', e => {
    // Get email and password inputs
    const email = inputEmail.value;
    const pass = inputPassword.value;
    const auth = firebase.auth();

    // Sign in
    const promise = auth.signInWithEmailAndPassword(email, pass);
    promise.catch(e => console.log(e.message));

  });

  // signup-event
  registerBtn.addEventListener('click', e => {

    //Get email and password inputs
    const email = inputEmail.value;
    const pass = inputPassword.value;
    const auth = firebase.auth();

    // create user
    const promise = auth.createUserWithEmailAndPassword(email, pass);
    promise.catch(e => console.log(e.message));
  });

  //log out button
  signOutBtn.addEventListener('click', e => {
    firebase.auth().signOut();
  });

  //add a realtime auth listener
  firebase.auth().onAuthStateChanged(firebaseUser => {
      if(firebaseUser){
        console.log(firebaseUser);
        console.log("Logged in!");
        signOutBtn.classList.remove('hide');
      } else {
        console.log('not logged in!');
        signOutBtn.classList.add('hide');
      }

  });


}());
