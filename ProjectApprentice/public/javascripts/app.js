(function() {
  var config = {
    apiKey: "AIzaSyA6633g9oUGKDZWXQrHXEV0ltqDNtpXCR0",
    authDomain: "project-apprentice.firebaseapp.com",
    databaseURL: "https://project-apprentice.firebaseio.com",
    storageBucket: "project-apprentice.appspot.com",
    messagingSenderId: "978941437309"
  };
  firebase.initializeApp(config);

  const btnS = document.getElementById('signOutBtn'); //log out
  const btnR = document.getElementById('regBtn'); //register
  const btn = document.getElementById('signInBtn'); //log in
  const inputEmail = document.getElementById('inputEmail');
  const inputPassword = document.getElementById('inputPassword');


  //log in user
  btn.addEventListener('click', e => {
    const email = inputEmail.value;
    const pass = inputPassword.value;
    const auth = firebase.auth();

    const promise = auth.signInWithEmailAndPassword(email, pass);
    promise.catch(e => console.log(e.message));
  });


  //Register user
  btnR.addEventListener('click', e => {
    const email = inputEmail.value;
    const pass = inputPassword.value;
    const auth = firebase.auth();
    const user = firebase.auth().currentUser;

    const promise = auth.createUserWithEmailAndPassword(email, pass);
    promise.catch(e => console.log(e.message));

  });


  //sign out
  btnS.addEventListener('click', e => {
    console.log("signed out!");
    const promise = firebase.auth().signOut();
    promise.catch(e => console.log(e.message));

  });

  //Realtime listener
  firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser) {
      console.log(firebaseUser);
    } else {
      console.log('not logged in');
    }
  });
}());
